{
  "parser": "@typescript-eslint/parser",
  "extends": [
    "airbnb",
    "plugin:import/errors",
    "prettier/react",
    "prettier/standard",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/eslint-recommended"
  ],
  "plugins": ["react", "import", "react-hooks", "prettier"],
  "parserOptions": {
    "ecmaVersion": 2020,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "settings": {
    "react": {
      "version": "detect"
    },
    "import/extensions": [".js", ".jsx", ".ts", ".tsx", ".mjs"],
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx"],
        "moduleDirectory": ["node_modules", "./"]
      }
    }
  },
  "rules": {
    "import/prefer-default-export": "off",
    "no-param-reassign": ["error", { "props": true, "ignorePropertyModificationsFor": ["state"] }],
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": ["error"],
    "jsx-a11y/label-has-associated-control": ["error", {
      "required": {
        "some": ["nesting", "id"]
      }
    }],
    "jsx-a11y/label-has-for": ["error", {
      "required": {
        "some": ["nesting", "id"]
      }
    }],
    "prettier/prettier": "error",
    "import/extensions": [1, "never"],
    "no-console": 1,
    "max-len": [
      "error",
      {
        "code": 120,
        "ignoreUrls": true,
        "ignoreComments": true
      }
    ],
    "no-unexpected-multiline": "error",
    "react/button-has-type": [
      "off",
      {
        "button": true,
        "submit": true,
        "reset": true
      }
    ],
    "react/require-default-props": "off",
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": ["error"],
    "react/destructuring-assignment": ["error", "always"],
    "react-hooks/rules-of-hooks": "error",
    "react/react-in-jsx-scope": "off",
    "react/jsx-first-prop-new-line": [2, "multiline"],
    "@typescript-eslint/no-non-null-assertion": "off",
    "react/jsx-filename-extension": [2, { "extensions": [".js", ".jsx", ".ts", ".tsx"] }],
    "react/jsx-props-no-spreading": "off",
    "camelcase": ["off", {"allow": ["released_on", "imdb_rating"]}]
  }
}
