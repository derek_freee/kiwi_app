import { configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import storeReducer from './store.reducer';

const store = configureStore({
  reducer: storeReducer,
});

export type IStore = ReturnType<typeof storeReducer>;
export type IDispatch = typeof store.dispatch;

export const useAppDispatch = (): IDispatch =>
  useDispatch<IDispatch>();

export default store;
