import { getFlights } from 'app/resources/api';
import { IGetFlightsParams } from 'app/resources/api.interface';
import { IDispatch } from '../store';
import flightsSlice from './flights.slice';

export const fligts = (params: IGetFlightsParams) => async (
  dispatch: IDispatch,
): Promise<void> => {
  dispatch(flightsSlice.actions.startLoading());
  try {
    const response = await getFlights(params);
    dispatch(flightsSlice.actions.flightsSuccess(response));
  } catch (error) {
    dispatch(flightsSlice.actions.hasError(error));
  }
};
