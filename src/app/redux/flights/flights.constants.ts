import { IFlightsState } from './flights.interface';

export const flightsInitalState: IFlightsState = {
  flights: [],
  currency: 'EUR',
  isLoading: false,
  hadSearch: false,
  error: false,
};
