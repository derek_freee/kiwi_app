import { IFlight } from 'app/types';

export interface IFlightsState {
  flights: IFlight[];
  currency: string;
  isLoading: boolean;
  hadSearch: boolean;
  error: boolean;
}
