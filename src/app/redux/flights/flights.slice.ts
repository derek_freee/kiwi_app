import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IResponseFlight } from 'app/types';
import { flightsInitalState } from './flights.constants';

const flightsSlice = createSlice({
  name: 'flights',
  initialState: flightsInitalState,
  reducers: {
    startLoading: (state) => {
      state.isLoading = true;
      state.hadSearch = true;
    },
    hasError: (state, { payload }: PayloadAction<boolean>) => {
      state.error = payload;
      state.isLoading = false;
    },
    flightsSuccess: (
      state,
      { payload: { data, currency } }: PayloadAction<IResponseFlight>,
    ) => {
      state.flights = data;
      state.currency = currency;
      state.isLoading = false;
    },
  },
});

export default flightsSlice;
