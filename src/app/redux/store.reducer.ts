import { combineReducers } from 'redux';
import flightsSlice from './flights/flights.slice';

const storeReducer = combineReducers({
  flights: flightsSlice.reducer,
});

export default storeReducer;
