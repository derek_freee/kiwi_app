export enum LocationTypes {
  AIRPORT = 'airport',
  CITY = 'city',
  COUNTRY = 'country',
}

export enum Patners {
  skypicker = 'skypicker',
}

export interface IRoute {
  last_seen: number;
  refresh_timestamp: number;
  aTime: number;
  dTime: number;
  aTimeUTC: number;
  dTimeUTC: number;
  cityTo: string;
  cityFrom: string;
  cityCodeFrom: string;
  cityCodeTo: string;
  flyTo: string;
  flyFrom: string;
  latFrom: number;
  lngFrom: number;
  latTo: number;
  lngTo: number;
  flight_no: number;
}

export interface IResponseFlight {
  data: IFlight[];
  currency: string;
}

export interface IFlight {
  id: string;
  dTime: number;
  dTimeUTC: number;
  aTime: number;
  aTimeUTC: number;
  nightsInDest: number;
  duration: {
    departure: number;
    return: number;
    total: number;
  };
  fly_duration: string;
  flyFrom: string;
  cityFrom: string;
  cityCodeFrom: string;
  countryFrom: {
    code: string;
    name: string;
  };
  flyTo: string;
  cityTo: string;
  cityCodeTo: string;
  countryTo: {
    code: string;
    name: string;
  };
  distance: number;
  price: number;
  availability: {
    seats: number;
  };
  conversion: {
    EUR: number;
  };
  deep_link: string;
  return_duration: string;
  route: IRoute[];
}

export interface ILocation {
  id: string;
  int_id: string;
  active: boolean;
  code: string;
  name: string;
  city: {
    id: string;
    name: string;
    code: string;
    slug: string;
  };
  location: {
    lon: number;
    lat: number;
  };
}
