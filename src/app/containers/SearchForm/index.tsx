import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';
import DatePicker from 'app/components/DatePicker';
import LocationPicker from '../../components/LocationPicker';
import useSearchForm from './hooks/useSearchForm';
import useSearchFormStyles from './hooks/useSearchFormStyles';

function SearchForm(): JSX.Element {
  const classes = useSearchFormStyles();
  const {
    changeFlyFromHandler,
    changeFlyToHandler,
    changeDateToHandler,
    submitHandler,
    formParams,
    hasFlyFrom,
  } = useSearchForm();
  const { t } = useTranslation(['common']);

  return (
    <form className={classes.form} onSubmit={submitHandler}>
      <Grid alignItems="flex-end" container spacing={2}>
        <Grid item xs={12} sm={6} md>
          <LocationPicker
            label={t('common:search_form.from')}
            isError={!hasFlyFrom}
            onChange={changeFlyFromHandler}
          />
        </Grid>
        <Grid item xs={12} sm={6} md>
          <LocationPicker
            label={t('common:search_form.to')}
            onChange={changeFlyToHandler}
          />
        </Grid>
        <Grid item>
          <DatePicker
            label={t('common:search_form.departure')}
            value={formParams.dateFrom}
            onChange={changeDateToHandler}
          />
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            disabled={!hasFlyFrom}
            type="submit"
          >
            {t('common:search_form.button')}
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default SearchForm;
