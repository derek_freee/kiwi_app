import { useState } from 'react';
import { useAppDispatch } from 'app/redux/store';
import { fligts } from 'app/redux/flights/flights.actions';
import { IGetFlightsParams } from 'app/resources/api.interface';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { defaultSearchFormState } from './useSearchForm.constants';
import { IUseSearchForm } from './useSearchForm.interface';

function useSearchForm(): IUseSearchForm {
  const dispatch = useAppDispatch();
  const [formParams, setFormParams] = useState<IGetFlightsParams>(
    defaultSearchFormState,
  );
  const changeFlyFromHandler = (flyFrom?: string) =>
    setFormParams((state) => ({ ...state, flyFrom }));
  const changeFlyToHandler = (flyTo?: string) =>
    setFormParams((state) => ({ ...state, flyTo }));
  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(fligts(formParams));
  };
  const changeDateToHandler = (dateFrom: MaterialUiPickersDate) =>
    setFormParams((state) => ({ ...state, dateFrom }));
  const hasFlyFrom = !!formParams.flyFrom;

  return {
    changeFlyFromHandler,
    changeFlyToHandler,
    changeDateToHandler,
    submitHandler,
    formParams,
    hasFlyFrom,
  };
}

export default useSearchForm;
