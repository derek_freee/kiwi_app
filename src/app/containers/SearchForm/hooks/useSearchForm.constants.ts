import { Patners } from 'app/types';

export const defaultSearchFormState = {
  partner: Patners.skypicker,
  dateFrom: new Date(),
};
