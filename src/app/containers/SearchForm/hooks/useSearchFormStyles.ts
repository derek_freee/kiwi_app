import { makeStyles } from '@material-ui/core/styles';

const useSearchFormStyles = makeStyles(() => ({
  form: {
    marginBottom: '15px',
  },
}));

export default useSearchFormStyles;
