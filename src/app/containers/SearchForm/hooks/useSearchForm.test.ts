import { act, renderHook } from '@testing-library/react-hooks';
import { useAppDispatch } from 'app/redux/store';
import useSearchForm from './useSearchForm';

jest.mock('app/redux/store');

describe('useSearchForm', () => {
  const defaultCountryCode = 'NY';
  const dispatchMock = jest.fn();
  const renderUseSearchForm = () =>
    renderHook(() => useSearchForm()).result;

  beforeEach(() => useAppDispatch.mockReturnValue(dispatchMock));

  test('should return current time', () => {
    const result = renderUseSearchForm();

    expect(result.current.formParams.dateFrom?.getDay).toBe(
      new Date().getDay,
    );
  });

  test('should return fly from code, and flag has fly true', () => {
    const result = renderUseSearchForm();

    act(() => {
      result.current.changeFlyFromHandler(defaultCountryCode);
    });

    expect(result.current.formParams.flyFrom).toBe(
      defaultCountryCode,
    );
    expect(result.current.hasFlyFrom).toBe(true);
  });

  test('should return fly to code', () => {
    const result = renderUseSearchForm();

    act(() => {
      result.current.changeFlyToHandler(defaultCountryCode);
    });

    expect(result.current.formParams.flyTo).toBe(defaultCountryCode);
  });

  test('should return sate date', async () => {
    const result = renderUseSearchForm();
    const dateNow = new Date();

    act(() => {
      result.current.changeDateToHandler(dateNow);
    });

    expect(result.current.formParams.dateFrom).toBe(dateNow);
  });
});
