import { IGetFlightsParams } from 'app/resources/api.interface';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

export interface IUseSearchForm {
  changeFlyFromHandler: (value?: string) => void;
  changeFlyToHandler: (value?: string) => void;
  changeDateToHandler: (value: MaterialUiPickersDate) => void;
  submitHandler: (event: React.FormEvent<HTMLFormElement>) => void;
  formParams: IGetFlightsParams;
  hasFlyFrom: boolean;
}
