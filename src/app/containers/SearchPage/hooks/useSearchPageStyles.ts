import { makeStyles } from '@material-ui/core/styles';

const useSearchFormStyles = makeStyles(() => ({
  root: {
    maxWidth: '900px',
    margin: '0 auto',
  },
}));

export default useSearchFormStyles;
