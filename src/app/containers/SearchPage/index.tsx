import SearchForm from 'app/containers/SearchForm';
import ResultsFeed from 'app/containers/ResultsFeed';
import useSearchFormStyles from './hooks/useSearchPageStyles';

function SearchPage(): JSX.Element {
  const claases = useSearchFormStyles();

  return (
    <div className={claases.root}>
      <SearchForm />
      <ResultsFeed />
    </div>
  );
}

export default SearchPage;
