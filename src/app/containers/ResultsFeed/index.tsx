import FlyCard from 'app/components/FlyCard';
import ResultsFeedLoader from 'app/components/ResultsFeedLoader';
import { useTranslation } from 'react-i18next';
import Button from '@material-ui/core/Button';
import NoResultsFeed from 'app/components/NoResultsFeed';
import useResultsFeed from './hooks/useResultsFeed';

function ResultsFeed(): JSX.Element {
  const { t } = useTranslation(['common']);
  const {
    isLoading,
    hadSearch,
    flights,
    currency,
    cardLimit,
    clickLoadMoreHandler,
  } = useResultsFeed();

  if (!hadSearch) return <div />;

  if (isLoading) return <ResultsFeedLoader count={10} />;

  if (!flights.length) return <NoResultsFeed />;

  return (
    <>
      {flights
        .map((flight) => (
          <FlyCard
            flight={flight}
            currency={currency}
            key={`${flight.id}_${flight.aTime}`}
          />
        ))
        .slice(0, cardLimit)}
      <Button
        fullWidth
        onClick={clickLoadMoreHandler}
        color="default"
        variant="outlined"
        size="small"
      >
        {t('common:result_feed.load_more')}
      </Button>
    </>
  );
}

export default ResultsFeed;
