import { renderHook, act } from '@testing-library/react-hooks';
import * as reactRedux from 'react-redux';
import useResultsFeed from './useResultsFeed';
import { DEFAULT_CARD_LIMIT } from './useResultsFeed.constants';

describe('useResultsFeed', () => {
  const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
  const renderUseResultsFeed = () =>
    renderHook(() => useResultsFeed()).result;

  beforeEach(() => {
    useSelectorMock.mockClear();
  });

  test('should return default limit of cards', () => {
    const result = renderUseResultsFeed();

    expect(result.current.cardLimit).toBe(DEFAULT_CARD_LIMIT);
  });

  test('should return increase limit by two', () => {
    const result = renderUseResultsFeed();

    act(() => {
      result.current.clickLoadMoreHandler();
    });

    expect(result.current.cardLimit).toBe(DEFAULT_CARD_LIMIT * 2);
  });
});
