import { IFlightsState } from 'app/redux/flights/flights.interface';
import { IStore } from 'app/redux/store';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { IUseResultsFeed } from './useResultsFeed.inreface';
import { DEFAULT_CARD_LIMIT } from './useResultsFeed.constants';

function useResultsFeed(): IUseResultsFeed {
  const [cardLimit, setCardLimit] = useState(DEFAULT_CARD_LIMIT);
  const stateFlights = useSelector<IStore, IFlightsState>(
    (state) => state.flights,
  );
  const clickLoadMoreHandler = () => setCardLimit(cardLimit * 2);

  return {
    ...stateFlights,
    cardLimit,
    clickLoadMoreHandler,
  };
}

export default useResultsFeed;
