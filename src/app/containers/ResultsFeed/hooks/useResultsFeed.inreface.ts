import { IFlightsState } from 'app/redux/flights/flights.interface';

export interface IUseResultsFeed extends IFlightsState {
  clickLoadMoreHandler: () => void;
  cardLimit: number;
}
