import { Suspense } from 'react';
import { Provider } from 'react-redux';
import SearchPage from './containers/SearchPage';
import store from './redux/store';
import 'config/i18n';

function App(): JSX.Element {
  return (
    <Suspense fallback={<div>Loading..</div>}>
      <Provider store={store}>
        <div className="App">
          <SearchPage />
        </div>
      </Provider>
    </Suspense>
  );
}

export default App;
