import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { ILocation } from 'app/types';
import { useCallback } from 'react';
import useLocationPickerControl from './hooks/useLocationPickerControl';
import useLoadLocation from './hooks/useLoadLocation';
import useLocationAutocompleteControl from './hooks/useLocationAutocompleteControl';

interface ILocationPickerProps {
  onChange: (value?: string) => void;
  label: string;
  isError?: boolean;
}

function LocationPicker({
  onChange,
  label,
  isError,
}: ILocationPickerProps): JSX.Element {
  const {
    inputValue,
    changeInputHandler,
  } = useLocationPickerControl();
  const {
    getOptionLabelHandler,
    getOptionSelectedHandler,
  } = useLocationAutocompleteControl();
  const changeAutocompleteHandler = useCallback(
    (_, value: ILocation | null) => {
      onChange(value?.code);
    },
    [],
  );
  const { locations } = useLoadLocation(inputValue);

  return (
    <Autocomplete<ILocation>
      fullWidth
      autoHighlight
      size="small"
      getOptionLabel={getOptionLabelHandler}
      getOptionSelected={getOptionSelectedHandler}
      options={locations}
      autoComplete
      includeInputInList
      filterSelectedOptions
      // value={value}
      // value={value?.code}
      onChange={changeAutocompleteHandler}
      onInputChange={changeInputHandler}
      renderInput={(params) => (
        <TextField
          {...params}
          error={isError}
          label={label}
          variant="outlined"
          fullWidth
        />
      )}
      renderOption={(option) => option.name}
    />
  );
}

export default LocationPicker;
