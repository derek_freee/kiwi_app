import { useState, useCallback } from 'react';
import { IUseLocationPickerControl } from './useLocationPickerControl.interface';

function useLocationPickerControl(): IUseLocationPickerControl {
  const [inputValue, setInputValue] = useState<string>('');
  const changeInputHandler = useCallback(
    (_, newInputValue: string) => setInputValue(newInputValue),
    [],
  );

  return {
    inputValue,
    changeInputHandler,
  };
}

export default useLocationPickerControl;
