export interface IUseLocationPickerControl {
  inputValue: string;
  changeInputHandler: (
    event: React.ChangeEvent<Record<string, string>>,
    inputValue: string,
  ) => void;
}
