import { useState, useCallback, useEffect } from 'react';
import { getLocations } from 'app/resources/api';
import { ILocation } from 'app/types';
import debounce from 'lodash/debounce';
import {
  MAX_LOCATIONS_LOAD_ITEAMS,
  locationTypes,
} from './useLoadLocation.constants';
import { IUseLoadLocation } from './useLoadLocation.interface';

function useLoadLocation(inputValue: string): IUseLoadLocation {
  const [locations, setLocations] = useState<ILocation[]>([]);
  const loadLocations = async (term: string) => {
    const response = await getLocations({
      location_types: locationTypes,
      limit: MAX_LOCATIONS_LOAD_ITEAMS,
      active_only: true,
      term,
    });

    setLocations(response);
  };
  const debounceLoadLocations = useCallback(
    debounce(loadLocations, 1000),
    [],
  );

  useEffect(() => {
    debounceLoadLocations(inputValue);
  }, [inputValue]);

  return {
    locations,
  };
}

export default useLoadLocation;
