import { ILocation } from 'app/types';

export interface IUseLoadLocation {
  locations: ILocation[];
}
