import { LocationTypes } from 'app/types';

export const MAX_LOCATIONS_LOAD_ITEAMS = 10;

export const locationTypes = [
  LocationTypes.AIRPORT,
  LocationTypes.CITY,
  LocationTypes.COUNTRY,
];
