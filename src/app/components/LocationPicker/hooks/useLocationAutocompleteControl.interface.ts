import { ILocation } from 'app/types';

export interface IUseLocationAutocompleteControl {
  getOptionLabelHandler: (location: ILocation) => string;
  getOptionSelectedHandler: (
    location: ILocation,
    value: ILocation,
  ) => boolean;
}
