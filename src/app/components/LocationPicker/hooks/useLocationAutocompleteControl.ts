import { useCallback } from 'react';
import { ILocation } from 'app/types';
import { IUseLocationAutocompleteControl } from './useLocationAutocompleteControl.interface';

function useLocationAutocompleteControl(): IUseLocationAutocompleteControl {
  const getOptionLabelHandler = useCallback(
    (location: ILocation) => location.name,
    [],
  );
  const getOptionSelectedHandler = useCallback(
    (location: ILocation, value: ILocation) =>
      location.id === value.id,
    [],
  );

  return {
    getOptionLabelHandler,
    getOptionSelectedHandler,
  };
}

export default useLocationAutocompleteControl;
