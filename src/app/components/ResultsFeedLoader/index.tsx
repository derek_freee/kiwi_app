import Skeleton from '@material-ui/lab/Skeleton';
import useResultsFeedLoaderStyles from './hooks/useResultsFeedLoaderStyles';

interface ResultsFeedLoaderProps {
  count: number;
}

function ResultsFeedLoader({
  count = 1,
}: ResultsFeedLoaderProps): JSX.Element {
  const classes = useResultsFeedLoaderStyles();

  return (
    <>
      {Array(count)
        .fill('')
        .map(() => (
          <Skeleton
            className={classes.card}
            key={Math.random()}
            variant="rect"
            height={152}
          />
        ))}
    </>
  );
}

export default ResultsFeedLoader;
