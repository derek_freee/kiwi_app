import { makeStyles } from '@material-ui/core/styles';

const useResultsFeedLoaderStyles = makeStyles(() => ({
  card: {
    marginBottom: '10px',
  },
}));

export default useResultsFeedLoaderStyles;
