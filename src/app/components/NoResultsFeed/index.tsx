import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import useNoResultsFeedStyles from './hooks/useNoResultsFeedStyles';

function NoResultsFeed(): JSX.Element {
  const classes = useNoResultsFeedStyles();
  const { t } = useTranslation(['common']);

  return (
    <Grid className={classes.root} container justify="center">
      <Typography variant="h5">
        {t('common:no_result.title')}
      </Typography>
      <Typography className={classes.subtitle} variant="subtitle2">
        {t('common:no_result.subtitle')}
      </Typography>
    </Grid>
  );
}

export default NoResultsFeed;
