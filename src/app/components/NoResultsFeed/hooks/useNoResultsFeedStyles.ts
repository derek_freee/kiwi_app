import { makeStyles } from '@material-ui/core/styles';

const useNoResultsFeedStyles = makeStyles((theme) => ({
  root: {
    padding: '50px 0',
  },
  subtitle: {
    paddingTop: '10px',
    color: theme.palette.grey[600],
  },
}));

export default useNoResultsFeedStyles;
