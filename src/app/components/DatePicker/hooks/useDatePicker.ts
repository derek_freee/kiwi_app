import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { IUseDatePicker } from './useDatePicker.interface';

function useDatePicker(
  onChange: (value: MaterialUiPickersDate) => void,
): IUseDatePicker {
  const changeDateHandler = (newDate: MaterialUiPickersDate): void =>
    onChange(newDate);
  const minDate = new Date();

  return {
    changeDateHandler,
    minDate,
  };
}

export default useDatePicker;
