import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

export interface IUseDatePicker {
  changeDateHandler: (
    date: MaterialUiPickersDate | null,
    value?: string | null,
  ) => void;
  minDate: Date;
}
