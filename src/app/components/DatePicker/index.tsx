import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { LOCATION_DATE_FORMAT } from 'config/constants';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import useDatePicker from './hooks/useDatePicker';

interface IDatePickerProps {
  onChange: (value: MaterialUiPickersDate) => void;
  value: MaterialUiPickersDate;
  label: string;
}

function DatePicker({
  onChange,
  value,
  label,
}: IDatePickerProps): JSX.Element {
  const { changeDateHandler, minDate } = useDatePicker(onChange);

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        minDate={minDate}
        format={LOCATION_DATE_FORMAT}
        label={label}
        value={value}
        onChange={changeDateHandler}
      />
    </MuiPickersUtilsProvider>
  );
}

export default DatePicker;
