import { makeStyles } from '@material-ui/core/styles';

const useFlyCardStyles = makeStyles((theme) => ({
  card: {
    marginBottom: '10px',
  },
  duration: {
    margin: '10px 0',
  },
  cityCode: {
    color: theme.palette.grey[600],
  },
}));

export default useFlyCardStyles;
