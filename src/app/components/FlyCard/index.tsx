import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { useTranslation } from 'react-i18next';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { IFlight } from 'app/types';
import { format } from 'date-fns';
import { LOCATION_DATE_FORMAT } from 'config/constants';
import AirplanemodeActiveIcon from '@material-ui/icons/AirplanemodeActive';
import Chip from '@material-ui/core/Chip';
import useFlyCardStyles from './hooks/useFlyCardStyles';

interface IFlyCardProps {
  flight: IFlight;
  currency: string;
}

function FlyCard({ flight, currency }: IFlyCardProps): JSX.Element {
  const classes = useFlyCardStyles();
  const { t } = useTranslation(['common']);

  return (
    <Card className={classes.card} variant="outlined">
      <CardContent>
        <Grid alignItems="center" justify="space-between" container>
          <Grid item>
            <Typography
              color="textSecondary"
              variant="caption"
              gutterBottom
            >
              {format(flight.aTime, LOCATION_DATE_FORMAT)}
            </Typography>
            <Grid
              spacing={1}
              alignContent="space-between"
              alignItems="baseline"
              container
            >
              <Grid item>
                <Typography variant="body1">
                  {flight.cityFrom}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  className={classes.cityCode}
                  variant="caption"
                >
                  {flight.cityCodeFrom}
                </Typography>
              </Grid>
            </Grid>
            <Chip
              className={classes.duration}
              icon={<AirplanemodeActiveIcon fontSize="small" />}
              size="small"
              variant="outlined"
              color="primary"
              label={flight.fly_duration}
            />
            <Grid
              spacing={1}
              alignContent="space-between"
              alignItems="baseline"
              container
            >
              <Grid item>
                <Typography variant="body1">
                  {flight.cityTo}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  className={classes.cityCode}
                  variant="caption"
                >
                  {flight.cityCodeTo}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Button
              href={flight.deep_link}
              target="_blank"
              color="secondary"
              variant="outlined"
              size="small"
            >
              {t('common:fly_card.buy')} {flight.price} {currency}
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default FlyCard;
