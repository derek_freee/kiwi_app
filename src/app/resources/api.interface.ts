import { LocationTypes, Patners } from 'app/types';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

/**
 * Get locations
 */
export interface IGetLocationsParams {
  term: string;
  location_types?: LocationTypes[];
  limit?: number;
  active_only: boolean;
}

/**
 * Get flights
 */
export interface IGetFlightsParams {
  flyFrom?: string;
  flyTo?: string;
  dateFrom: MaterialUiPickersDate;
  partner: Patners;
}
