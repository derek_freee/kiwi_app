import axios from 'axios';

const apiServiceOptions = {
  baseURL: process.env.REACT_APP_API_DOMAIN,
};
const apiService = axios.create(apiServiceOptions);

export default apiService;
