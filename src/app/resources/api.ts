import { ILocation, IResponseFlight } from 'app/types';
import { format } from 'date-fns';
import { LOCATION_DATE_FORMAT_REQUEST } from 'config/constants';
import { LOCATIONS, FLIGHTS } from './api.constants';
import API from './services/apiService';
import {
  IGetFlightsParams,
  IGetLocationsParams,
} from './api.interface';

/**
 * Get locations
 */
export const getLocations = (
  params: IGetLocationsParams,
): Promise<ILocation[]> =>
  API.get(LOCATIONS, { params }).then(({ data }) => data.locations);

/**
 * Get flights
 */
export const getFlights = ({
  flyFrom,
  flyTo,
  dateFrom,
  partner,
}: IGetFlightsParams): Promise<IResponseFlight> =>
  API.get(FLIGHTS, {
    params: {
      fly_from: flyFrom,
      fly_to: flyTo,
      date_from: dateFrom
        ? format(dateFrom, LOCATION_DATE_FORMAT_REQUEST)
        : null,
      partner,
    },
  }).then(({ data: { data, currency } }) => ({
    data,
    currency,
  }));
